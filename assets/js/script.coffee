angular.module 'app', [
  'sc.app.helpers'
]

.factory 'Templates', ->
  cobranca:
    show: 'templates/show.html'
    form: 'templates/form.html'

.controller 'Financeiro::Cobranca::IndexCtrl', [
  '$scope', 'Templates', 'CobrancaService'
  (s, Templates, CobrancaService)->
    s.template   = Templates
    s.cobrancas  = CobrancaService.cobrancas

    s.tiposDestinoUnidades = (cob)->
      CobrancaService.tipo_destino_unidades[cob.destino]

    s.destinoComNome = (cob)->
      destino = s.tiposDestinoUnidades(cob)
      nomePagador = cob.nome
      "#{destino} - #{nomePagador}"

    s.oldAcc = undefined
    s.toggleAcc = (acc)->
      if s.oldAcc && s.oldAcc != acc
        s.oldAcc.close()
      acc.toggle()
      s.oldAcc = acc
]

.controller 'Financeiro::Cobranca::ItemCtrl', [
  '$scope', 'scToggle'
  (s, toggler)->
    s.init = (cob)->
      s.cobranca = cob
      s.cobranca.acc = new toggler
        onClose: -> s.formulario.close()
      s.formulario = new toggler
        onOpen: -> s.cobranca.acc.open()

    s.nomePagador = ->
      nome = pag.nome for pag in s.cobranca.pagadores when pag.id == s.cobranca.pagador_id
      nome
]

.controller 'Financeiro::Cobranca::FormCtrl', [
  '$scope'
  (s)->
    s.init = (cob)->
      s.form = angular.copy cob

    s.setDestino = ->
      s.form.destino = pag.destino for pag in s.form.pagadores when pag.id == s.form.pagador_id
]

.factory 'CobrancaService', ->
  tipo_destino_unidades: {
    'locatario':    'Morador'
    'proprietario': 'Proprietário'
  }
  cobrancas: [
    {
      titulo: 'Taxa de Condomínio'
      status: 'green'
      valor_original: 95
      vencimento: new Date('04-15-2015')
      mes_referencia: new Date('04-01-2015')
      created_at: new Date('04-01-2015')
      destino: 'proprietario'
      pagador_id: 2
      composicoes: [
        {
          titulo: 'Rateio Janeiro'
          valor: 167.89
        }
        {
          titulo: 'Reforma da calçada'
          valor: 54.32
        }
        {
          titulo: 'Multa por Barulho'
          valor: 95
        }
      ]
      unidade: {
        id: 1
        nome: 'Apt 101'
      }
      pagadores: [
        {
          id: 1
          nome: 'Fulano'
          destino: 'locatario'
        }
        {
          id: 2
          nome: 'Ciclano'
          destino: 'proprietario'
        }
      ]
    }
    {
      titulo: 'Taxa de Condomínio'
      status: 'blue'
      valor_original: 95
      vencimento: new Date('04-16-2015')
      mes_referencia: new Date('04-01-2015')
      created_at: new Date('04-01-2015')
      destino: 'locatario'
      pagador_id: 1
      composicoes: [
        {
          titulo: 'Rateio Janeiro'
          valor: 167.89
        }
        {
          titulo: 'Reforma da calçada'
          valor: 54.32
        }
        {
          titulo: 'Multa por Barulho'
          valor: 95
        }
      ]
      unidade: {
        id: 1
        nome: 'Apt 102'
      }
      pagadores: [
        {
          id: 1
          nome: 'Fulano'
          destino: 'locatario'
        }
        {
          id: 2
          nome: 'Ciclano'
          destino: 'proprietario'
        }
      ]
    }
    {
      titulo: 'Ressarcimento'
      status: 'red'
      valor_original: 106
      vencimento: new Date('04-17-2015')
      mes_referencia: new Date('04-01-2015')
      created_at: new Date('04-01-2015')
      pagador_id: 3
      composicoes: [
        titulo: 'Produto com defeito'
        valor: 106
      ]
      pagadores: [
        {
          id: 3
          nome: 'Casas Bahia'
        }
      ]
    }
  ]
